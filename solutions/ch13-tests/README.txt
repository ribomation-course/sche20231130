# Build

    cmake -G Ninja -S . -B bld
    cmake --build bld
    cmake --build bld --target test
    ./bld/unit-test
    ./bld/unit-test -s
    ctest --test-dir bld
    ( cd bld && make test )

# Clean

    rm -rf bld


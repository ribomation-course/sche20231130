#include "numbers.hxx"

unsigned long fibonacci(unsigned n) {
	if (n == 0) return 0;
	if (n == 1) return 1;
	return fibonacci(n-2) + fibonacci(n-1);
}

unsigned long sum(unsigned n) {
	return n * (n + 1) / 2;
}


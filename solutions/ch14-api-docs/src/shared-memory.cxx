#include <string>
#include <stdexcept>
#include <cstring>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include "shared-memory.hxx"

namespace ribomation::shm {
    using namespace std;
    using namespace std::literals;

    SharedMemory::SharedMemory(unsigned long size_) : size{static_cast<unsigned>(size_)} {
        int fd = shm_open(name.c_str(), O_RDWR | O_CREAT | O_TRUNC, 0660);
        if (fd < 0) sysfail("shm_open");

        int rc = ftruncate(fd, size);
        if (rc < 0) sysfail("ftruncate");

        auto start = mmap(nullptr, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
        if (start == MAP_FAILED) sysfail("mmap");
        close(fd);

        SharedMemory::begin = start;
        SharedMemory::end   = start + size;
        SharedMemory::next  = start;
    }

    SharedMemory::~SharedMemory() {
        void* addr = const_cast<void*>(begin);
        munmap(addr, size);
        shm_unlink(name.c_str());
    }

    void* SharedMemory::allocateBytes(unsigned numBytes) {
        void* addr = next;
        next += numBytes;
        if (end < next) {
            throw overflow_error("shared-memory overflow");
        }
        return addr;
    }

    void SharedMemory::sysfail(const string& func) {
        string msg = func + "() failed: " + strerror(errno);
        throw runtime_error(msg);
    }


}


#include <iostream>
#include "just_resting.hxx"
using namespace std;
using namespace std::literals;
using namespace just_resting;

int main() {
    auto srv = Application{};
    srv.debug(true);
    srv.port(3500, true);

    srv.route("GET"s, "/"s, [](Request& req, Response& res) {
        res.body("Hi there, from a justRESTing microservice"s);
    });

    srv.launch([](auto port){
      cout << "server started. http://localhost:" << port << endl;
    });
    
}

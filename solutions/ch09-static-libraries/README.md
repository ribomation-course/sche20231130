
# Shapes

    cmake -G Ninja -S shapes -B bld/shapes
    cmake --build bld/shapes --target run -- -v

# Shared Memeory

    cmake -G Ninja -S shm -B bld/shm
    cmake --build bld/shm --target run -- -v

# Clean

    rm -rf bld


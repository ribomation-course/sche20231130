#include <iostream>
#include <iomanip>
#include <string>
#include "build-info.hxx"
using namespace std;
using namespace std::literals;

int main() {
	cout << "App name   : " << BuildInfo::appName() << endl;
	cout << "App version: " << BuildInfo::appVersion() << endl;
	
	cout << "Host       : " << BuildInfo::hostName() << endl;
	cout << "OS         : " << BuildInfo::osName() << endl;
	cout << "Processor  : " << BuildInfo::processorName() << endl;

	cout << "Build date : " << BuildInfo::buildDate() << endl;
	cout << "User name  : " << BuildInfo::userName() << endl;
	cout << "Commit ID  : " << BuildInfo::commitId() << endl;
}


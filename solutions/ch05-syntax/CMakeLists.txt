cmake_minimum_required(VERSION 3.12)
project(hello LANGUAGES NONE)

message(STATUS "CMAKE_CURRENT_SOURCE_DIR = ${CMAKE_CURRENT_SOURCE_DIR}")
message(STATUS "CMAKE_CURRENT_BINARY_DIR = ${CMAKE_CURRENT_BINARY_DIR}")
message(STATUS "CMAKE_GENERATOR          = ${CMAKE_GENERATOR}")
message(STATUS "PROJECT_NAME             = ${PROJECT_NAME}")
message(STATUS "CMAKE_HOST_SYSTEM        = ${CMAKE_HOST_SYSTEM}")
message(STATUS "CMAKE_BUILD_TYPE         = ${CMAKE_BUILD_TYPE}")

message("Tjolla hopp")

set(msg "Foobar strikes again")
message(STATUS "msg = ${msg}")

set(answer 42)
message(WARNING "answer = ${answer}")

message(STATUS "user.name = $ENV{USER}")
message(STATUS "user.home = $ENV{HOME}")
message(STATUS "shell     = $ENV{SHELL}")


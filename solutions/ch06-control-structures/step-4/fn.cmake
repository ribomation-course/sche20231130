function(numbers)
    message("--- numbers ---")
    foreach(k RANGE 1 7)
        message("${k}")
    endforeach()
endfunction()

function(words)
    message("--- words ---")
    foreach(k IN ITEMS foo bar tjolla hopp)
        message("${k}")
    endforeach()
endfunction()


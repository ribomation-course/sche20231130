# Configure
    cmake -B bld/1 -S step-1
    cmake -B bld/2 -S step-2
    cmake -B bld/3 -S step-3
    cmake -B bld/4 -S step-4

# Clean
    rm -rf bld

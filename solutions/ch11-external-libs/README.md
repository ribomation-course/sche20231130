# Build & Run

    cmake -G Ninja -S . -B bld
    cmake --build bld -- -v
    cmake --build bld --target run

# Clean

    rm -rf bld


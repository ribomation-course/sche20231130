
# 1) Default

    cmake -S . -B bld/default
    cmake --build bld/default -- VERBOSE=1
    cmake --build bld/default --target run

# 2) Build Type: Release

    cmake -DCMAKE_BUILD_TYPE=Release -S . -B bld/release
    cmake --build bld/release -- VERBOSE=1
    cmake --build bld/release --target run

# 2b) Build Type: Debug

    cmake -DCMAKE_BUILD_TYPE=Debug -S . -B bld/debug
    cmake --build bld/debug -- VERBOSE=1
    cmake --build bld/debug --target run

# 3) Build Tool: Ninja

    cmake -G Ninja -S . -B bld/ninja
    cmake --build bld/ninja -- -v
    cmake --build bld/ninja --target run

# 4) Compiler: CLang C/C++

    cmake -DCMAKE_CXX_COMPILER=clang++-16 -DCMAKE_C_COMPILER=clang-16 -S . -B bld/clang
    cmake --build bld/clang -- VERBOSE=1
    cmake --build bld/clang --target run

# 4b) Compiler: Ninja & CLang C/C++

    cmake -DCMAKE_CXX_COMPILER=clang++-16 -DCMAKE_C_COMPILER=clang-16 -G Ninja -S . -B bld/ninja-clang
    cmake --build bld/ninja-clang -- -v
    cmake --build bld/ninja-clang --target run


# Clean

    rm -rf bld



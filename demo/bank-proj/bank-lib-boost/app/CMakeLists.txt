add_executable(bank)
target_sources(bank PRIVATE src/bank.cxx)
target_link_libraries(bank PRIVATE accounts Boost::program_options)


#include <iostream>
#include <string>
#include <random>
#include <vector>
#include <boost/program_options.hpp>
#include "account.hxx"

using std::cout;
using std::string;
using namespace std::string_literals;
namespace opts = boost::program_options;
using ribomation::Account;

int main(int argc, char** argv) {
	auto options = opts::options_description{"Allowed options"};
	options.add_options()
		("help,?", "display help info")
		("count,n", opts::value<unsigned>()->default_value(5), "number of accounts to create")
	;

	auto variables = opts::variables_map{};
	try {
		opts::store(opts::parse_command_line(argc, argv, options), variables);
		opts::notify(variables);
	} catch (opts::error& err) {
		std::cerr << "Failure: " << err.what() << "\n";
		return 1;
	}
	if (variables.count("help")) {
		cout << options << "\n";
		return 0;
	}

	auto r = std::random_device{};
	auto randDigit = std::uniform_int_distribution<char>{'0', '9'};
	auto randAccno = [&] {
		auto digits = ""s;
		auto n = 8;
		while (n-- > 0) digits += randDigit(r);
		return "erb-"s + digits + "-sek"s;
	};
	auto randBalance = std::normal_distribution<float>{1000, 500};

	auto numAccounts = variables["count"].as<unsigned>();
	auto accounts    = std::vector<Account>{};
	accounts.reserve(numAccounts);
	while (numAccounts-- > 0)
		accounts.emplace_back(randAccno(), randBalance(r));

	for (auto const& a : accounts) cout << a << "\n";
}


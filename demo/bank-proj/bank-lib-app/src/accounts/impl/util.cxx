#include <string>
#include <algorithm>
#include <cctype>
#include "util.hxx"

auto toUpperCase(std::string s) -> std::string {
	std::transform(s.begin(), s.end(), s.begin(), [](auto ch){
		return ::toupper(ch);		
	});
	return s;
}


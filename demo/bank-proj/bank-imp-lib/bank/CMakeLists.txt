cmake_minimum_required(VERSION 3.18)
project(bank LANGUAGES CXX)
set(CMAKE_CXX_STANDARD 17)

# -- accounts lib --
set(ACCOUNTS_DIST "${CMAKE_CURRENT_LIST_DIR}/../external/dist")
add_library(accounts STATIC IMPORTED)
set_target_properties(accounts PROPERTIES
    IMPORTED_LOCATION ${ACCOUNTS_DIST}/libaccounts.a
)
target_include_directories(accounts
    INTERFACE   ${ACCOUNTS_DIST}
)

# -- bank exe --
add_executable(bank)
target_sources(bank
    PRIVATE src/bank.cxx
)
target_link_libraries(bank PRIVATE accounts)

# -- run --
add_custom_target(run 
    COMMAND bank
)


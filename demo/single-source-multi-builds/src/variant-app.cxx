#include <iostream>
#include <string>
#ifdef CONFIG
    #include "config.hxx"
#endif

using namespace std;
using namespace std::literals;

auto variant() {
#if defined(VARIANT)
    #if VARIANT == 1
        return "This is variant: FIRST"s;
    #elif VARIANT == 2
        return "This is variant: SECOND"s;
    #elif VARIANT == 3
        return "This is variant: THIRD"s;
    #else
        return "Unsupported variant: "s + to_string(VARIANT);
    #endif
#else
    return "No variant defined"s;
#endif
}

int main() {
    cout << "App: build with a variant config:\n    "
         << variant() 
#ifdef CONFIG
        << "\n    The Answer=" << theAnswer
#endif
         << endl;
    return 0;
}

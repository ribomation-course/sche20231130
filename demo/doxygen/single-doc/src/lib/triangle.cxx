#include <iostream>
#include <sstream>
#include "triangle.hxx"

using namespace std;
using namespace std::literals;

namespace ribomation::shapes {
    Triangle::Triangle(int base, int height)
            : super{"triang"}, base{base}, height{height} {
        cout << "triang{" << base << ", " << height << "} @ " << this << endl;
    }

    Triangle::~Triangle() {
        cout << "~Triangle() @ " << this << endl;
    }

    double Triangle::area() const {
        return base * height / 2.0;
    }

    string Triangle::toString() const {
        ostringstream buf;
        buf << super::toString() << "{" << base << ", " << height << "}";
        return buf.str();
    }
}

#pragma once

#include <string>
#include <iosfwd>

namespace ribomation::shapes {
    class Shape {
        std::string type;
    protected:
        explicit Shape(const std::string& type);
    public:
        virtual ~Shape();
        virtual double area() const = 0;

        virtual const std::string& getType() const final { return type; }

        virtual std::string toString() const;
    };

    inline std::ostream& operator<<(std::ostream& os, const Shape& s) {
        return os << s.toString();
    }

}
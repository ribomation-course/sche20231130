cmake_minimum_required(VERSION 3.18)
project(doxygen_minimal)

set(CMAKE_CXX_STANDARD 17)
set(WARN -Wall -Wextra -Werror -Wfatal-errors)

file(GLOB FILES src/*.hxx src/*.cxx)
foreach(file ${FILES})
    message(STATUS ${file})
endforeach()

add_executable(shapes)
target_sources(shapes PRIVATE ${FILES})
target_compile_options(shapes PRIVATE ${WARN})
target_include_directories(shapes PRIVATE src)

find_package(Doxygen)
if (DOXYGEN_FOUND)
    doxygen_add_docs(docs src)
endif (DOXYGEN_FOUND)


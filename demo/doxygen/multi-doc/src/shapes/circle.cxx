#include <iostream>
#include <sstream>
#include "circle.hxx"

using namespace std;
using namespace std::literals;

namespace ribomation::shapes {
    Circle::Circle(int radius)
            : super{"circ"}, radius{radius} {
        cout << "circ{" << radius << "} @ " << this << endl;
    }

    Circle::~Circle() {
        cout << "~Circle() @ " << this << endl;
    }

    double Circle::area() const {
        return PI * radius * radius;
    }

    string Circle::toString() const {
        ostringstream buf;
        buf << super::toString() << "{" << radius << "}";
        return buf.str();
    }
}

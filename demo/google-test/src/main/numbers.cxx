#include "numbers.hxx"

auto Numbers::fibonacci(unsigned n) -> unsigned long long {
    if (n == 0) return 0;
    if (n == 1) return 1;
    return fibonacci(n - 2) + fibonacci(n - 1);
}


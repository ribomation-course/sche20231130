# Installation Instructions

In order to participate and perform the programming exercises of the course,
you need to have the following installed.

## Zoom Client
* https://us02web.zoom.us/download
* [Video/Audio Configuration](https://gitlab.com/ribomation-course/common-instructions/-/blob/master/zoom-configuration.md)

## GIT Client
* https://git-scm.com/downloads

## A (Ubuntu) Linux system
* C/C++ compiler
* CMake, Make, Ninja

We will do the exercises on Linux. Therefore, you need access to a Linux or Unix system, preferably Ubuntu.
Read our common guide of how to install Linux on WSL and a C++ compiler.

* [Linux and C++ Installation](https://gitlab.com/ribomation-course/common-instructions/-/blob/master/linux-and-cxx.md)


### Installation within *NIX
Install the latest version of CMake, by downloading the `*.tar.gz` file, unpack it and follow the instructions in Readme.txt.
* https://cmake.org/download/

In addition, you need to install (_the commands below are for Ubuntu Linux_)
* The Make and Ninja builder tools; `sudo apt install make ninja-build`
* The GNU C/C++ compiler, version 11 or later; `sudo apt install gcc-11 g++-11`
* The CLang C++ compiler, version 14 or later; `sudo apt install clang`
* Git client; `sudo apt install git`
* Any text editor you like, such as `nano`, `emacs`, `vim`


